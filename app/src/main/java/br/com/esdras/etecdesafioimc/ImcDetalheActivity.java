package br.com.esdras.etecdesafioimc;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.common.util.UriUtil;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.io.File;

public class ImcDetalheActivity extends AppCompatActivity {

    ImageView imageViewResultado;
    TextView textViewIMC, textViewFicaDica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc_detalhe);

        imageViewResultado = findViewById(R.id.imageViewResultado);
        textViewIMC = findViewById(R.id.textViewIMC);
        textViewFicaDica = findViewById(R.id.textViewFicaADica);

        final double imc = getIntent().getDoubleExtra("imc", 18D);

        if (imc < 18) {
            imageViewResultado.setImageResource(R.drawable.feminino);
            textViewIMC.setText(getString(R.string.obesidade_um));
            textViewFicaDica.setText(getString(R.string.obesidade_um_dica));
        }

    }
}