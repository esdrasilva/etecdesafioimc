package br.com.esdras.etecdesafioimc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    TextInputLayout mTextInputLayoutAltura, mTextInputLayoutPeso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextInputLayoutAltura = findViewById(R.id.textInputLayoutAltura);
        mTextInputLayoutPeso = findViewById(R.id.textInputLayoutPeso);

        Button mButton = findViewById(R.id.button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validarCampos()) {
                    double altura = Double.parseDouble(Objects.requireNonNull(mTextInputLayoutAltura.getEditText()).getText().toString());
                    double peso = Double.parseDouble(Objects.requireNonNull(mTextInputLayoutPeso.getEditText()).getText().toString());

                    double imc = peso / (altura * altura);

                    TextView textView = findViewById(R.id.textViewResultado);
                    textView.setText(String.format("Seu IMC é %.2f", imc));

                    Intent intent = new Intent(getApplicationContext(), ImcDetalheActivity.class);
                    intent.putExtra("imc", imc);
                    startActivity(intent);
                }
            }
        });

    }

    private boolean validarCampos() {
        boolean isAlturaValida, isPesoValido = false;

        if (Objects.requireNonNull(mTextInputLayoutPeso.getEditText()).getText().toString().isEmpty()) {
            mTextInputLayoutPeso.setErrorEnabled(true);
            mTextInputLayoutPeso.setError("Digite o peso");
        } else {
            mTextInputLayoutPeso.setErrorEnabled(false);
            isPesoValido = true;
        }

        if (Objects.requireNonNull(mTextInputLayoutAltura.getEditText()).getText().toString().isEmpty()) {
            mTextInputLayoutAltura.setErrorEnabled(true);
            mTextInputLayoutAltura.setError("Digite a altura");
            isAlturaValida = false;
        } else {
            mTextInputLayoutAltura.setErrorEnabled(false);
            isAlturaValida = true;
        }

        return isPesoValido && isAlturaValida;
    }
}